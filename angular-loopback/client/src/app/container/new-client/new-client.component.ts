import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-new-client',
  templateUrl: './new-client.component.html',
  styleUrls: ['./new-client.component.scss']
})
export class NewClientComponent implements OnInit {
  id: any;
  constructor(private router: ActivatedRoute) { 
    this.id = this.router.snapshot.paramMap.get('id');//get id from url
  }

  ngOnInit(): void {
    //console.log(this.id);
    
  }

}
