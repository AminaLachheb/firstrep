import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './container/main/main.component';
import { ClientComponent } from './component/client/client.component';
import { NavComponent } from './component/nav/nav.component';
import { NewClientComponent } from './container/new-client/new-client.component';
import { FormClientComponent } from './component/form-client/form-client.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    ClientComponent,
    NavComponent,
    NewClientComponent,
    FormClientComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
