import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './container/main/main.component';
import { NewClientComponent } from './container/new-client/new-client.component';

const routes: Routes = [
{
  path: '',
  component: MainComponent
},
{
  path: 'new',
  component: NewClientComponent
},
{
  path: 'edit/:id',
  component: NewClientComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
