export class Client {
    id: string;
    name: string;
    city: string;
    phoneNumber: 0;
    email: string;
    commercialRecord: string;
}
