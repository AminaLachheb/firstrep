import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Client } from 'src/app/model/client';
import { DatabaseService } from 'src/app/service/database.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-client',
  templateUrl: './form-client.component.html',
  styleUrls: ['./form-client.component.scss']
})
export class FormClientComponent implements OnInit {
  client: Client = new Client();
  @Input() id: string;
  constructor(public databaseService : DatabaseService, private router: Router) { }

  ngOnInit(): void {
    if(this.id){
      this.getOneClient();
    }
  }
  send(form: NgForm){
    if(form.invalid){
      Swal.fire(
        'error',
        'Invalid data',
        'error'
      );
    }
    if(this.id){
      this.editClient(form);
    }
    else{
    this.post(form);
    }
  }
  post(form: NgForm){
    this.databaseService.postClient(form.value).subscribe(
      res => {
        Swal.fire(
          'Success',
          'Client created',
          'success'
        );
        this.router.navigateByUrl('/');
      },
      err => {
        console.error(err);
      }
    )
  }
  getOneClient(){
    this.databaseService.getOneClient(this.id).subscribe(
    (client: Client)=>{
        this.client = client;
    },
    err => {
      console.error(err);
      
    });
  }
  editClient(form: NgForm){
    this.databaseService.putClient(form.value, this.id)
    .subscribe(res =>{
      Swal.fire(
        'Success',
        'Client updated',
        'success'
      );
      this.router.navigateByUrl('/');
    },
    err => {
      console.error(err);
    });
  }
}
