import { Component, Input, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Client } from 'src/app/model/client';
import { DatabaseService } from 'src/app/service/database.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss']
})
export class ClientComponent implements OnInit {
  SearchClients: Client[];
  clients: Client[];

  constructor(private databaseService: DatabaseService) { }

  ngOnInit(): void {
    this.getClients();
  }
  getClients(){
    this.databaseService.getClient().subscribe(
      (clients: Client[])=> {
        this.SearchClients = this.clients = clients;        
        },
        err => {
          console.error(err);
        }
      );
  }
  deleteClient(client: Client){
    const id = client.id;

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.databaseService.deleteClient(id).subscribe(
          res =>{
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
            location.reload();
          },
          err => {
            console.error(err); 
          }
        );
      }
    })
  }

  search(fSearch: string){
    
     this.SearchClients = (fSearch) ? 
     this.clients.filter(client => client.city.toLowerCase().includes(fSearch.toLowerCase()))
     : this.clients;
  }
}
