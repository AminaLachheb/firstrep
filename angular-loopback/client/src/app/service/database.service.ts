import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Client } from '../model/client';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  private readonly URL: string = 'http://localhost:3000/clients';

  constructor(private http:HttpClient) { }

  getClient(){
    return this.http.get(this.URL);
  }

  postClient(client: Client){
    return this.http.post(this.URL, client);
  }

  putClient(client: Client, id: string){
    return this.http.put(this.URL+`/${id}`, client);
  }
  deleteClient(id: string){
    return this.http.delete(this.URL+`/${id}`);
  }
  getOneClient(id: string){
    return this.http.get(this.URL+`/${id}`);
  }
}
